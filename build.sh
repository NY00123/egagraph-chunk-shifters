#!/bin/sh
gcc -g --std=c99 egagraph_pic_shifter.c bmp256_mod.c -o egagraph_pic_shifter `pkg-config sdl3 --cflags --libs`
gcc -g --std=c99 egagraph_picm_shifter.c bmp256_mod.c -o egagraph_picm_shifter `pkg-config sdl3 --cflags --libs`
gcc -g --std=c99 egagraph_sprite_shifter.c bmp256_mod.c -o egagraph_sprite_shifter `pkg-config sdl3 --cflags --libs`
gcc -g --std=c99 egagraph_tile8_shifter.c bmp256_mod.c -o egagraph_tile8_shifter `pkg-config sdl3 --cflags --libs`
gcc -g --std=c99 egagraph_tile8m_shifter.c bmp256_mod.c -o egagraph_tile8m_shifter `pkg-config sdl3 --cflags --libs`

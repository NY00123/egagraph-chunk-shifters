#!/bin/sh
gcc -g --std=c99 egagraph_pic_shifter.c bmp256_mod.c -o egagraph_pic_shifter `sdl2-config --cflags --libs` -DUSE_SDL2=1
gcc -g --std=c99 egagraph_picm_shifter.c bmp256_mod.c -o egagraph_picm_shifter `sdl2-config --cflags --libs` -DUSE_SDL2=1
gcc -g --std=c99 egagraph_sprite_shifter.c bmp256_mod.c -o egagraph_sprite_shifter `sdl2-config --cflags --libs` -DUSE_SDL2=1
gcc -g --std=c99 egagraph_tile8_shifter.c bmp256_mod.c -o egagraph_tile8_shifter `sdl2-config --cflags --libs` -DUSE_SDL2=1
gcc -g --std=c99 egagraph_tile8m_shifter.c bmp256_mod.c -o egagraph_tile8m_shifter `sdl2-config --cflags --libs` -DUSE_SDL2=1

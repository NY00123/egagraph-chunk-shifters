#include <stdio.h>
#include <stdlib.h>

#include "egagraph_shifters_sdl_compat_defs.h"
#include "bmp256_mod.h"

typedef char id0_char_t;
typedef uint8_t id0_byte_t;
typedef int16_t id0_int_t;
typedef uint16_t id0_unsigned_t;
typedef int32_t id0_long_t;
#define id0_huge

#define SPRITE_TABLE_OFFSET 2 // HARDCODED, but generally doesn't change

#define BASIS_WINDOW_WIDTH 512
#define BASIS_WINDOW_HEIGHT 512
//#define BASIS_WINDOW_WIDTH (320*2)
//#define BASIS_WINDOW_HEIGHT (200*3)

typedef struct {
  id0_unsigned_t bit0,bit1;	// 0-255 is a character, > is a pointer to a node
} __attribute__((__packed__)) huffnode;

// From ID_VW.H in Cat3D/KDreams; Other code pieces are from TED5.
typedef struct {
  id0_int_t	width,
	height,
	orgx,orgy,
	xl,yl,xh,yh,
	shifts;
} __attribute__((__packed__)) spritetabletype;

// Additional info

typedef struct {
	int32_t egagraphbyteoffset;
	int32_t egagraphbitmask;
} egagraphexactloctype;

typedef struct {
	egagraphexactloctype egagraphplanesoffsets[5];
	int/*bool*/ isinit;
} spriteplaneptrstabletype;

static uint8_t *egagraph;
static huffnode *egadict;
static int32_t *ega4head; // After conversion from 3-bytes EGAHEAD format to 4-bytes

static spritetabletype *spritetable;
static spriteplaneptrstabletype *spriteplaneptrstable;

static int32_t egagraphsize;
static int nOfEntries;

static SDL_Window *g_window;
static SDL_Renderer *g_renderer;

// WARNING - Assumes Little-Endian order
#define OFF3(m,i)	(*(id0_long_t id0_huge *)((id0_char_t id0_huge *)m+(i)*3)&0xffffff)

static void HuffExpand (id0_byte_t id0_huge *source, id0_byte_t id0_huge *sourceEnd, id0_byte_t id0_huge *dest,
  id0_long_t length,huffnode *hufftable, id0_byte_t bitmask, egagraphexactloctype *exactlocs, int expectedPartSize, int partsLeft) {
	id0_unsigned_t code;
	huffnode *headptr = hufftable+254; // head node is always node 254
	huffnode *nodeon = headptr;

	//------------
	// expand data
	//------------

	// Ported from ASM and modified to suit our needs
	id0_byte_t id0_huge *srcptr = source, *dstptr = dest, *dstendptr = dest+length;
	id0_byte_t byteval = *(srcptr++); // load first byte
	//id0_byte_t bitmask = 1;
	int i = 0;
	id0_byte_t  id0_huge *nextdstpartptr = dstptr;
	do {
		// optionally update these
		if (partsLeft && (dstptr == nextdstpartptr)) {
			nextdstpartptr += expectedPartSize;
			exactlocs->egagraphbyteoffset = (srcptr-1) - egagraph; // HACK - accessing egagraph directly
			exactlocs->egagraphbitmask = bitmask;
			++exactlocs;
			--partsLeft;
		}

		// take bit0 or bit1 path from node
		code = (byteval & bitmask) ? nodeon->bit1 : nodeon->bit0;
		if (bitmask & 0x80) {
			// NEW - Safety measurement
			if (srcptr == sourceEnd) {
				break;
			}
			byteval = *(srcptr++); // load next byte
			bitmask = 1; // back to first bit
		} else {
			bitmask <<= 1; // advance to next bit position
		}
		// if < 256 it's a byte, else move node
		if (code >= 256) {
			// NOTE: Not using OptimizeNodes
			nodeon = hufftable + (code-256);
			continue;
		}
		*(dstptr++) = code; // write a decompressed byte out
		nodeon = headptr; // back to the head node for next bit

		if (dstptr == dstendptr) { // done?
			break;
		}
	} while (1);
}

static void ExpandGrChunk(void **ptr, int i, egagraphexactloctype *exactlocs, int expectedPartSize, int partsLeft) {
	// Covering only sprite table and sprites
	int32_t offset = ega4head[i];
	int32_t decompSize = *(int32_t *)(egagraph+offset);
	// Just in case ...
	decompSize = (decompSize > 1000000 ? decompSize : 1000000);
	//
	*ptr = malloc(decompSize);
	if (!(*ptr)) {
		printf("Can't allocate memory for chunk no. %d!\n", i);
	}
	HuffExpand(egagraph+offset+4, egagraph+egagraphsize, (id0_byte_t *)*ptr, decompSize, egadict, 1, exactlocs, expectedPartSize, partsLeft);
}

static int32_t FileLengthFromHandle(FILE *fp) {
	long int origOffset = ftell(fp);
	fseek(fp, 0, SEEK_END);
	long int len = ftell(fp);
	fseek(fp, origOffset, SEEK_SET);
	return len;
}

static void LoadFileToMem(const char *filename, void **ptr, long *len) {
	FILE *fp = fopen(filename, "rb");
	if (!fp) {
		fprintf(stderr, "Can't open %s!\n", filename);
		exit(3);
	}
	*len = FileLengthFromHandle(fp);
	*ptr = malloc(*len);
	if (!(*ptr)) {
		fprintf(stderr, "Can't allocate memory for file %s!\n", filename);
		exit(4);
	}
	if (fread(*ptr, *len, 1, fp) != 1) {
		fprintf(stderr, "Failed to properly read data from file %s!\n", filename);
		exit(5);
	}
	fclose(fp);
}

static void LoadAllFiles(const char *ext) {
	long templen;
	uint8_t *temphead;
	char filename[13];
	snprintf(filename, sizeof(filename), "EGAGRAPH.%s", ext);
	LoadFileToMem(filename, (void **)&egagraph, &templen);
	egagraphsize = templen;
	snprintf(filename, sizeof(filename), "EGADICT.%s", ext);
	LoadFileToMem(filename, (void **)&egadict, &templen);
	snprintf(filename, sizeof(filename), "EGAHEAD.%s", ext);
	LoadFileToMem(filename, (void **)&temphead, &templen);

	nOfEntries = templen/3;

	ega4head = (int32_t *)malloc(nOfEntries*4);
	if (!ega4head) {
		fprintf(stderr, "Can't allocate memory for ega4head!\n");
		exit(6);
	}

	for (long i = 0; i < nOfEntries; ++i) {
		ega4head[i] = OFF3(temphead, i);
	}

	free(temphead);
}

static /*const*/ uint32_t bgraEGAColors[] = {
	0xff000000/*black*/, 0xff000088/*blue*/, 0xff008800/*green*/, 0xff008888/*cyan*/,
	0xff880000/*red*/, 0xff880088/*magenta*/, 0xff884400/*brown*/, 0xff888888/*light gray*/,
	0xff444444/*gray*/, 0xff4444cc/*light blue*/, 0xff44cc44/*light green*/, 0xff44cccc/*light cyan*/,
	0xffcc4444/*light red*/, 0xffcc44cc/*light magenta*/, 0xffcccc44/*yellow*/, 0xffcccccc/*white*/,
};

static const uint32_t bgraEGAMaskColor = 0xff999900; /*some shade of yellow*/

static int GetPixelByPlane(const uint8_t *spriteData, int x, int y, int w, int h, int plane) {
	return (*(spriteData+plane*h*w/8+y*w/8+x/8) & (1<<(7-(x%8)))) != 0;
}

static void PrepareDefaultSpritesDataOffsets(int spritesOffset, int totalNumOfSprites) {
	void *spriteData;
	int/*bool*/ isExpandingAnyChunk = 0;
	for (int i = 0; i < totalNumOfSprites; ++i) {
		if (!spriteplaneptrstable[i].isinit) {
			spritetabletype *s = &spritetable[i];
			if (!isExpandingAnyChunk) {
				printf("Need to temporarily expand at least one sprite chunk, may take a while...\n");
				isExpandingAnyChunk = 1;
			}
			// Temporarily expand data, for filling with default offsets
			//printf("PrepareDefaultSpritesDataOffsets - Getting offsets for sprite %d\n", i);
			ExpandGrChunk((void **)&spriteData, i + spritesOffset, spriteplaneptrstable[i].egagraphplanesoffsets, s->width*s->height, 5);
			free(spriteData);
			spriteplaneptrstable[i].isinit = 1;
		}
	}
}

static const char *g_offsetsFileName = "egagraph_sprite_shifter_offsets.txt";

static void LoadSpritesDataOffsets(void) {
	FILE *fp = fopen(g_offsetsFileName, "rb");
	if (!fp) {
		return;
	}

	char buffer[80];
	while (fgets(buffer, sizeof(buffer), fp)) {
		int spriteNum;
		egagraphexactloctype exactlocsCopy[5];
		if (sscanf(buffer, "%d %d %d %d %d %d %d %d %d %d %d",
			&spriteNum,
			&exactlocsCopy[0].egagraphbyteoffset, &exactlocsCopy[0].egagraphbitmask,
			&exactlocsCopy[1].egagraphbyteoffset, &exactlocsCopy[1].egagraphbitmask,
			&exactlocsCopy[2].egagraphbyteoffset, &exactlocsCopy[2].egagraphbitmask,
			&exactlocsCopy[3].egagraphbyteoffset, &exactlocsCopy[3].egagraphbitmask,
			&exactlocsCopy[4].egagraphbyteoffset, &exactlocsCopy[4].egagraphbitmask
		) == 11) {
			memcpy(spriteplaneptrstable[spriteNum].egagraphplanesoffsets, exactlocsCopy, sizeof(exactlocsCopy));
			spriteplaneptrstable[spriteNum].isinit = 1;
		}
	}

	fclose(fp);
}

static void SaveSpritesDataOffsets(int totalNumOfSprites) {
	FILE *fp = fopen(g_offsetsFileName, "wb");
	if (!fp) {
		return;
	}

	for (int spriteNum = 0; spriteNum < totalNumOfSprites; ++spriteNum) {
		egagraphexactloctype *exactlocs = spriteplaneptrstable[spriteNum].egagraphplanesoffsets;
		fprintf(fp, "%d %d %d %d %d %d %d %d %d %d %d\n",
			spriteNum,
			exactlocs[0].egagraphbyteoffset, exactlocs[0].egagraphbitmask,
			exactlocs[1].egagraphbyteoffset, exactlocs[1].egagraphbitmask,
			exactlocs[2].egagraphbyteoffset, exactlocs[2].egagraphbitmask,
			exactlocs[3].egagraphbyteoffset, exactlocs[3].egagraphbitmask,
			exactlocs[4].egagraphbyteoffset, exactlocs[4].egagraphbitmask
		);
	}

	fclose(fp);
}

static void SaveAllSpritesAsBitmaps(int totalNumOfSprites) {
	for (int i = 0; i < totalNumOfSprites; ++i) {
		printf("SaveAllSpritesAsBitmaps - Exporting sprite %d...\n", i);

		const spritetabletype *s = &spritetable[i];
		int w=s->width * 8;
		int h=s->height;

		BITMAP256 *bmp = bmp256_create(w, h, 8);
		if (!bmp) {
			fprintf(stderr, "SaveAllSpritesAsBitmaps - bmp256_create failed!\n (Sprite %d)", i);
			exit(7);
		}

		// Expand planes separately
		uint8_t *spriteData = (uint8_t *)malloc(5*s->width*s->height);
		if (!(spriteData)) {
			fprintf(stderr, "Can't allocate memory for sprite no. %d!\n", i);
			exit(8);
		}
		for (int p = 0; p < 5; ++p) {
			HuffExpand(egagraph+spriteplaneptrstable[i].egagraphplanesoffsets[p].egagraphbyteoffset, egagraph+egagraphsize, spriteData+p*s->width*s->height, s->width*s->height, egadict, spriteplaneptrstable[i].egagraphplanesoffsets[p].egagraphbitmask, NULL, 0, 0);

			for (int y = 0; y < h; ++y) {
				for (int x = 0; x < w; ++x) {
					if (!p) {
						bmp->lines[y][x] = (GetPixelByPlane(spriteData,x,y,w,h,p)) << 4;
					} else {
						bmp->lines[y][x] = (GetPixelByPlane(spriteData,x,y,w,h,p)) << (p-1);
					}
				}
			}
			char filename[64];
			snprintf(filename, sizeof(filename), "egagraph_sprite_%04d_p%d.bmp", i, p);
			bmp256_save(bmp, filename);
		}

		free(spriteData);
		bmp256_free(bmp);
	}
	printf("SaveAllSpritesAsBitmaps - Done.\n");
}

static SDL_Texture *LoadSpriteToTexture(int i, int *pW, int *pH) {
	spritetabletype *s = &spritetable[i];
	int w=s->width * 8;
	int h=s->height;
	SDL_Texture *texture = SDL_CreateTexture(g_renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, 5*w, 3*h);
	if (!texture) {
		fprintf(stderr, "Can't create texture for sprite %d!\n", i);
		exit(6);
	}
	SDL_SetTextureScaleMode(texture, SDL_SCALEMODE_NEAREST);

	// Expand planes separately
	uint8_t *spriteData = (uint8_t *)malloc(5*s->width*s->height);
	if (!(spriteData)) {
		printf("Can't allocate memory for sprite no. %d!\n", i);
	}
	for (int p = 0; p < 5; ++p) {
		HuffExpand(egagraph+spriteplaneptrstable[i].egagraphplanesoffsets[p].egagraphbyteoffset, egagraph+egagraphsize, spriteData+p*s->width*s->height, s->width*s->height, egadict, spriteplaneptrstable[i].egagraphplanesoffsets[p].egagraphbitmask, NULL, 0, 0);
	}

	uint32_t *pixels;
	int pitch;
	SDL_LockTexture(texture, NULL, (void **)&pixels, &pitch);
	// First fill whole texture
	memset(pixels, 0x9f, 5*w*3*h*4); // HACK

	for (int i = 0; i < h; ++i) {
		for (int j = 0; j < w; ++j, ++pixels) {
			int pixVal = 0;
			uint32_t pixOffset = ((i ^ j) & 1) ? 0x00111111 : 0;
			for (int p = 0; p < 4; ++p) {
				pixVal |= (GetPixelByPlane(spriteData,j,i,w,h,p+1)) << p;
			}

			if (GetPixelByPlane(spriteData,j,i,w,h,0)) {
				*pixels = pixOffset + bgraEGAMaskColor;
				*(pixels + 2*w) = pixOffset + bgraEGAColors[15]; // Mask plane
			} else {
				*pixels = pixOffset + bgraEGAColors[pixVal];
				*(pixels + 2*w) = pixOffset + bgraEGAColors[0]; // Mask plane
			}
			// Fill planes 1-4 separately
			*(pixels + 4*w) = pixOffset + bgraEGAColors[pixVal & 1];
			*(pixels + 5*2*h*w) = pixOffset + bgraEGAColors[pixVal & 2];
			*(pixels + (5*2*h+2)*w) = pixOffset + bgraEGAColors[pixVal & 4];
			*(pixels + (5*2*h+4)*w) = pixOffset + bgraEGAColors[pixVal & 8];
		}
		pixels += 4*w;
	}

	SDL_UnlockTexture(texture);
	free(spriteData);
	*pW = 5*w;
	*pH = 3*h;
	return texture;
}

static void RefreshScreenWithTexture(int i) {
	int x, y;
	SDL_RenderClear(g_renderer);
	SDL_Rect rect;
	SDL_Texture *texture = LoadSpriteToTexture(i, &rect.w, &rect.h);
	int winWidth, winHeight;
	SDL_GetWindowSize(g_window, &winWidth, &winHeight);
#if 1
	SDL_RenderTexture(g_renderer, texture, NULL, NULL);
#else
	rect.x = ((BASIS_WINDOW_WIDTH - rect.w) / 2) * winWidth / BASIS_WINDOW_WIDTH;
	rect.y = ((BASIS_WINDOW_HEIGHT - rect.h) / 2) * winHeight / BASIS_WINDOW_HEIGHT;
	rect.w = rect.w * winWidth / BASIS_WINDOW_WIDTH;
	rect.h = rect.h * winHeight / BASIS_WINDOW_HEIGHT;
	SDL_RenderTexture(g_renderer, texture, NULL, &rect);
#endif
	SDL_RenderPresent(g_renderer);
	SDL_DestroyTexture(texture);
}

static void INCREASE_SPRITE_PLANE(int i, int p, int shiftCount) {
	egagraphexactloctype *exactloc = &spriteplaneptrstable[i].egagraphplanesoffsets[p];
	while (shiftCount-- > 0) {
		if (exactloc->egagraphbitmask & 0x80) {
			++(exactloc->egagraphbyteoffset);
			exactloc->egagraphbitmask = 1;
		} else {
			exactloc->egagraphbitmask <<= 1;
		}
	}
}

static void DECREASE_SPRITE_PLANE(int i, int p, int shiftCount) {
	egagraphexactloctype *exactloc = &spriteplaneptrstable[i].egagraphplanesoffsets[p];
	while (shiftCount-- > 0) {
		if (exactloc->egagraphbitmask & 1) {
			--(exactloc->egagraphbyteoffset);
			exactloc->egagraphbitmask = 0x80;
		} else {
			exactloc->egagraphbitmask >>= 1;
		}
	}
}

int main(int argc, char *argv[]) {
	if (argc != 4) {
		fprintf(stderr, "Usage: %s <sprites_start> <num_of_sprites> <ext>\n", argv[0]);
		return 1;
	}
	int spritesOffset = atoi(argv[1]);
	int totalNumOfSprites = atoi(argv[2]);
	const char *ext = argv[3];
	if ((USE_SDL2 && (SDL_Init(SDL_INIT_VIDEO) < 0)) ||
	    (!USE_SDL2 && !SDL_Init(SDL_INIT_VIDEO))) {
		fprintf(stderr, "SDL_Init failed! %s\n", SDL_GetError());
		return 1;
	}
	SDL_Window *window;
	SDL_Renderer *renderer;
#if USE_SDL2
	if (SDL_CreateWindowAndRenderer(BASIS_WINDOW_WIDTH, BASIS_WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE, &window, &renderer) < 0)
#else
	if (!SDL_CreateWindowAndRenderer("Viewer", BASIS_WINDOW_WIDTH, BASIS_WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE, &window, &renderer))
#endif
	{
		fprintf(stderr, "SDL_CreateWindowAndRenderer failed! %s\n", SDL_GetError());
		return 2;
	}
#if USE_SDL2
	SDL_SetWindowTitle(window, "Viewer");
#endif
	g_window = window;
	g_renderer = renderer;

	SDL_SetRenderDrawColor(renderer, 127, 127, 127, 255);

	spriteplaneptrstable = (spriteplaneptrstabletype *)calloc(totalNumOfSprites,sizeof(spriteplaneptrstabletype)); // Using calloc ON PURPOSE!
	if (!spriteplaneptrstable) {
		fprintf(stderr, "Failed to allocate memory for spriteplaneptrstable!\n");
		return 3;
	}

	LoadAllFiles(ext);

	ExpandGrChunk((void **)&spritetable, SPRITE_TABLE_OFFSET, NULL, 0, 0);
	LoadSpritesDataOffsets();
	PrepareDefaultSpritesDataOffsets(spritesOffset, totalNumOfSprites);
#if 0
	for (int i = 0; i < 350; ++i) {
		spritetabletype *s = &spritetable[i];
		printf("%d: w %d h %d ox %d oy %d xl %d yl %d xh %d yh %d shifts %d\n",
		       i, s->width, s->height, s->orgx, s->orgy, s->xl, s->yl, s->xh, s->yh, s->shifts
		);
	}
#endif

	int spriteNum = 0;
	RefreshScreenWithTexture(spriteNum);

	SDL_Event event;
	do {
		int shiftCount;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
#if USE_SDL2
			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
				case SDL_WINDOWEVENT_EXPOSED:
					RefreshScreenWithTexture(spriteNum);
					break;
				}
				break;
#else
			case SDL_EVENT_WINDOW_RESIZED:
			case SDL_EVENT_WINDOW_EXPOSED:
				RefreshScreenWithTexture(spriteNum);
				break;
#endif
			case SDL_EVENT_KEY_DOWN:
				shiftCount = (EVENT_KEY(event).mod & SDL_KMOD_SHIFT) ? 32 : 1;
				switch (EVENT_KEY(event).scancode) {

				case SDL_SCANCODE_LEFT:
					if (spriteNum > 0) {
						--spriteNum;
						RefreshScreenWithTexture(spriteNum);
					}
					break;
				case SDL_SCANCODE_RIGHT:
					if (spriteNum < totalNumOfSprites-1) {
						++spriteNum;
						RefreshScreenWithTexture(spriteNum);
					}
					break;

				case SDL_SCANCODE_PAGEUP:
					if (spriteNum > 0) {
						spriteNum = (spriteNum < 30) ? 0 : (spriteNum - 30);
						RefreshScreenWithTexture(spriteNum);
					}
					break;
				case SDL_SCANCODE_PAGEDOWN:
					if (spriteNum < totalNumOfSprites - 1) {
						spriteNum = (totalNumOfSprites - 1 < spriteNum + 30) ? (totalNumOfSprites - 1) : (spriteNum + 30);
						RefreshScreenWithTexture(spriteNum);
					}
					break;

				case SDL_SCANCODE_Q:
					INCREASE_SPRITE_PLANE(spriteNum, 0, shiftCount);
					INCREASE_SPRITE_PLANE(spriteNum, 1, shiftCount);
					INCREASE_SPRITE_PLANE(spriteNum, 2, shiftCount);
					INCREASE_SPRITE_PLANE(spriteNum, 3, shiftCount);
					INCREASE_SPRITE_PLANE(spriteNum, 4, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_A:
					DECREASE_SPRITE_PLANE(spriteNum, 0, shiftCount);
					DECREASE_SPRITE_PLANE(spriteNum, 1, shiftCount);
					DECREASE_SPRITE_PLANE(spriteNum, 2, shiftCount);
					DECREASE_SPRITE_PLANE(spriteNum, 3, shiftCount);
					DECREASE_SPRITE_PLANE(spriteNum, 4, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_W:
					INCREASE_SPRITE_PLANE(spriteNum, 1, shiftCount);
					INCREASE_SPRITE_PLANE(spriteNum, 2, shiftCount);
					INCREASE_SPRITE_PLANE(spriteNum, 3, shiftCount);
					INCREASE_SPRITE_PLANE(spriteNum, 4, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_S:
					DECREASE_SPRITE_PLANE(spriteNum, 1, shiftCount);
					DECREASE_SPRITE_PLANE(spriteNum, 2, shiftCount);
					DECREASE_SPRITE_PLANE(spriteNum, 3, shiftCount);
					DECREASE_SPRITE_PLANE(spriteNum, 4, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_E:
					INCREASE_SPRITE_PLANE(spriteNum, 0, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_D:
					DECREASE_SPRITE_PLANE(spriteNum, 0, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_R:
					INCREASE_SPRITE_PLANE(spriteNum, 1, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_F:
					DECREASE_SPRITE_PLANE(spriteNum, 1, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_T:
					INCREASE_SPRITE_PLANE(spriteNum, 2, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_G:
					DECREASE_SPRITE_PLANE(spriteNum, 2, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_Y:
					INCREASE_SPRITE_PLANE(spriteNum, 3, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_H:
					DECREASE_SPRITE_PLANE(spriteNum, 3, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_U:
					INCREASE_SPRITE_PLANE(spriteNum, 4, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;
				case SDL_SCANCODE_J:
					DECREASE_SPRITE_PLANE(spriteNum, 4, shiftCount);
					RefreshScreenWithTexture(spriteNum);
					break;

				case SDL_SCANCODE_P:
					bgraEGAColors[6] ^= 0x0000cc00; // HACK
					RefreshScreenWithTexture(spriteNum);
					break;

				case SDL_SCANCODE_SLASH:
					SaveAllSpritesAsBitmaps(totalNumOfSprites);
					break;
				}
				break;
			case SDL_EVENT_KEY_UP:
				break;
			case SDL_EVENT_QUIT:
				goto finish;
			}
		}
		SDL_Delay(1);
	} while (1);

finish:
	SaveSpritesDataOffsets(totalNumOfSprites);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
